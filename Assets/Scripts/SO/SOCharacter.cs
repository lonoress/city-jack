using UnityEngine;

[CreateAssetMenu(fileName = "Character - ", menuName = "SO/Character", order = 1)]
public class SOCharacter : SO
{
    [SerializeField] private float _velocitySpeedIncrease;
    [SerializeField] private float _velocitySpeedDecrease;
    [SerializeField] private float _movementSpeed;
    [SerializeField] private float _rotationSpeed;
    [SerializeField] private float _jumpForce;
    [SerializeField] private float _animationMovementSpeed;
    
#if UNITY_EDITOR
    protected override void Reset()
    {
        base.Reset();
        _velocitySpeedIncrease = 4;
        _velocitySpeedDecrease = 6;
        _movementSpeed = 8f;
        _rotationSpeed = 10f;
        _jumpForce = 6f;
        _animationMovementSpeed = 4f;
    }
#endif

    
    public float GetVelocitySpeedIncrease() => _velocitySpeedIncrease;
    public float GetVelocitySpeedDecrease() => _velocitySpeedDecrease;
    public float GetMovementSpeed() => _movementSpeed;
    public float GetRotationSpeed() => _rotationSpeed;
    public float GetJumpForce() => _jumpForce;
    public float GetAnimationMovementSpeed() => _animationMovementSpeed;
}
