using UnityEngine;

public class Player : MonoBehaviour, ICollectableCollector
{
    [SerializeField] private Character _character;
    [SerializeField] private Inventory _inventory;

    public readonly Character.Event onCharacterRemoved = new Character.Event();
    public readonly Character.Event onCharacterChanged = new Character.Event();
    
    

    public Character GetCharacter() => _character;
    public Inventory GetInventory() => _inventory;

    public void MoveInGlobalDirection(Vector3 direction)
    {
        if (_character == null) return;
        _character.Move(direction);
    }

    public void Jump()
    {
        if (_character == null) return;
        _character.Jump();
    }

    public void SetCharacter(Character character)
    {
        RemoveCharacter();
        _character = character;
        onCharacterChanged.Invoke(_character);

        if (_character == null) return;
        _character.onTriggerEnter.AddListener(OnCharacterTriggerEnter);
    }

    private void RemoveCharacter()
    {
        if (_character == null) return;
        _character.onTriggerEnter.RemoveListener(OnCharacterTriggerEnter);
        onCharacterRemoved.Invoke(_character);
        _character = null;
    }

    public void Collect(ICollectable collectable)
    {
        if (collectable.CanBeCollected())
        {
            _inventory.Add(collectable.GetItemKind(), collectable.GetQuantity());
            collectable.Collect();
        }
    }

    private void OnCharacterTriggerEnter(Collider other)
    {
        ItemRenderer itemRenderer = other.GetComponent<ItemRenderer>();
        if (itemRenderer != null)
        {
            Collect(itemRenderer);
        }
    }
}
