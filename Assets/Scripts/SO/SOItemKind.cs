using UnityEngine;

[CreateAssetMenu(fileName = "Item Kind - ", menuName = "SO/Item Kind", order = 1)]
public class SOItemKind : SO
{
    [SerializeField] private AudioClip _audioOnCollect;

    public AudioClip GetAudioOnCollect() => _audioOnCollect;
}
