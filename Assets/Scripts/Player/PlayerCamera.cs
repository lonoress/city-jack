using UnityEngine;

public class PlayerCamera : BaseMonoBehaviour
{
    [SerializeField] private Transform _target;
    [Space]
    [SerializeField] private float _speed;
    [SerializeField] private Transform _rotatationHorizontal;
    [SerializeField] private Transform _rotatationVertical;
    [SerializeField] private Transform _zoom;
    [Space]
    [SerializeField] private float _verticalMin;
    [SerializeField] private float _verticalMax;
    [SerializeField] private float _zoomMin;
    [SerializeField] private float _zoomMax;
    private Update _update;
    
#if UNITY_EDITOR
    protected override void Reset()
    {
        base.Reset();
        _speed = 10f;
        _target = null;
        _verticalMin = 10f;
        _verticalMax = 80f;
        _zoomMin = -15f;
        _zoomMax = -5f;
    }
#endif

    public Transform GetDirectionTransform() => _rotatationHorizontal;

    public void SetTarget(Transform target)
    {
        _target = target;
    }

    public void ClearTarget()
    {
        _target = null;
    }

    private void LateUpdate()
    {
        if (_target == null) return;
        SetPosition(Vector3.Lerp(transform.position, _target.position, Time.deltaTime * _speed));
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    public void RotateHorizontally(float angle)
    {
        _rotatationHorizontal.Rotate(0f, angle, 0f);
    }
    
    public void RotateVertically(float angle)
    {
        Vector3 vector = _rotatationVertical.localEulerAngles;
        vector.x = Mathf.Clamp(vector.x - angle, _verticalMin, _verticalMax);
        _rotatationVertical.localEulerAngles = vector;
    }

    public void Zoom(float delta)
    {
        Vector3 pos = _zoom.localPosition;
        pos.z = Mathf.Clamp(pos.z + delta, _zoomMin, _zoomMax);
        _zoom.localPosition = pos;
    }
}
