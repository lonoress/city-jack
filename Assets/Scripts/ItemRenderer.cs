using UnityEngine;

public class ItemRenderer : BaseMonoBehaviour, ICollectable
{
    [SerializeField] private bool _collected;
    [SerializeField] private SOItemKind _sO;
    

    public SOItemKind GetItemKind()
    {
        return _sO;
    }

    public int GetQuantity()
    {
        return 1;
    }

    public bool CanBeCollected()
    {
        return !_collected;
    }

    public void Collect()
    {
        _collected = true;
        AudioSource.PlayClipAtPoint(_sO.GetAudioOnCollect(), transform.position);
        gameObject.SetActive(false);
    }
}
