
public interface ICollectable
{
    public SOItemKind GetItemKind();
    public int GetQuantity();
    public bool CanBeCollected();

    public void Collect();
}
