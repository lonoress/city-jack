using System;
using UnityEngine;
using UnityEngine.Events;

public class InputStandaloneKeyboard : BaseMonoBehaviour
{
    [SerializeField] private KeyCode[] _forward;
    [SerializeField] private KeyCode[] _backward;
    [SerializeField] private KeyCode[] _right;
    [SerializeField] private KeyCode[] _left;
    [SerializeField] private KeyCode[] _jump;
    [Space]
    [SerializeField] private MoveEvent _onMove;
    [SerializeField] private UnityEvent _onJump;

    [System.Serializable]
    public class MoveEvent : UnityEvent<MoveEventData> { }

    [System.Serializable]
    public struct MoveEventData
    {
        public MoveEventData(bool forward, bool backward, bool right, bool left)
        {
            this.forward = forward;
            this.backward = backward;
            this.right = right;
            this.left = left;
        }

        public readonly bool forward;
        public readonly bool backward;
        public readonly bool right;
        public readonly bool left;
    }
    
    private enum InputKind
    {
        Key,
        KeyDown,
        KeyUp
    }

#if UNITY_EDITOR
    protected override void Reset()
    {
        base.Reset();
        _forward = new[] {KeyCode.W, KeyCode.UpArrow};
        _backward = new[] {KeyCode.S, KeyCode.DownArrow};
        _right = new[] {KeyCode.D, KeyCode.RightArrow};
        _left = new[] {KeyCode.A, KeyCode.LeftArrow};
        _jump = new[] {KeyCode.Space};
    }
#endif
    
    void Update()
    {
        bool f = IsAnyKey(_forward, InputKind.Key);
        bool b = IsAnyKey(_backward, InputKind.Key);
        bool r = IsAnyKey(_right, InputKind.Key);
        bool l = IsAnyKey(_left, InputKind.Key);
        
        bool fb = !(f && b) && (f || b);
        bool rl = !(r && l) && (r || l);

        if (fb || rl)
        {
            MoveEventData ev = new MoveEventData(f, b, r, l);
            _onMove.Invoke(ev);
        }

        if (IsAnyKey(_jump, InputKind.KeyDown))
        {
            _onJump.Invoke();
        }
    }

    private bool IsAnyKey(KeyCode[] keys, InputKind kind)
    {
        int length = keys.Length;
        if (length == 0) return false;
        if (length == 1) return IsKey(keys[0], kind);
        for (int i = 0; i < length; i++)
        {
            if (IsKey(keys[i], kind))
            {
                return true;
            }
        }
        return false;
    }

    private bool IsKey(KeyCode key, InputKind kind)
    {
        switch (kind)
        {
            case InputKind.Key: return Input.GetKey(key);
            case InputKind.KeyDown: return Input.GetKeyDown(key);
            case InputKind.KeyUp: return Input.GetKeyUp(key);
            default: throw new NotImplementedException($"{nameof(InputKind)}.{kind} not implemented");
        }
    }
}
