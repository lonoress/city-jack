using UnityEngine;
using UnityEngine.Events;

public class InputStandaloneMouse : BaseMonoBehaviour
{
    [SerializeField] private string _axisX;
    [SerializeField] private string _axisY;
    [Space]
    [SerializeField] private MouseEvent _onMove;
    [SerializeField] private UnityEventVector2 _onScroll;
    
#if UNITY_EDITOR
    protected override void Reset()
    {
        base.Reset();
        _axisX = "Mouse X";
        _axisY = "Mouse Y";
    }
#endif
    
    [System.Serializable]
    public class MouseEvent: UnityEvent<MouseEventData>{}
    
    [System.Serializable]
    public struct MouseEventData
    {
        public MouseEventData(Axis raw, Axis normalized)
        {
            this.raw = raw;
            this.normalized = normalized;
        }
        
        public readonly Axis raw;
        public readonly Axis normalized;
    }

    [System.Serializable]
    public struct Axis
    {
        public Axis(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
        
        public readonly float x;
        public readonly float y;
    }

    private void Update()
    {
        UpdateMove();
        UpdateZoom();
    }

    private void UpdateMove()
    {
        // Read axis input
        float x = Input.GetAxis(_axisX);
        float y = Input.GetAxis(_axisY);

        // Nothing to declare
        if (x == 0f && y == 0f) return;
        
        Axis raw = new Axis(x, y);

        // Normalize
        float res = Mathf.Min(Screen.width, Screen.height);
        x /= res;
        y /= res;
        Axis normalized = new Axis(x, y);
        
        MouseEventData ev = new MouseEventData(raw, normalized);
        _onMove.Invoke(ev);
    }

    private void UpdateZoom()
    {
        Vector2 delta = Input.mouseScrollDelta;
        if (delta.x == 0f && delta.y == 0f) return;
        _onScroll.Invoke(delta);
    }
}
