using UnityEngine;

public abstract class SO : ScriptableObject
{
#if UNITY_EDITOR
    protected virtual void Reset(){ }

    protected virtual void OnValidate(){ }

    protected virtual void Awake(){ }

    protected virtual void OnEnable(){ }

    protected virtual void OnDisable(){ }

    protected virtual void OnDestroy(){ }
#endif
}
