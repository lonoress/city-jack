using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Inventory : MonoBehaviour
{
    private readonly Dictionary<SOItemKind, int> Map = new Dictionary<SOItemKind, int>();
    
    public readonly UnityEvent onChanged = new UnityEvent();

    public int GetQuantity(SOItemKind kind)
    {
        if (Map.ContainsKey(kind)) return Map[kind];
        return 0;
    }

    public void Add(SOItemKind kind, int quantity)
    {
        if (Map.ContainsKey(kind))
        {
            Map[kind] = Map[kind] + quantity;
        }
        else
        {
            Map.Add(kind, quantity);
        }
        
        onChanged.Invoke();
    }
}
