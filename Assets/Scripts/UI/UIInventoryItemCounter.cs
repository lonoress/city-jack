using TMPro;
using UnityEngine;

public class UIInventoryItemCounter : BaseMonoBehaviour
{
    [SerializeField] private SOItemKind _kind;
    [SerializeField] private TMP_Text _tmpValue;
    [SerializeField] private Inventory _inventory;

    protected override void Start()
    {
        base.Start();
        _inventory.onChanged.AddListener(OnPlayerCharacterCollected);
        UpdateLook();
    }

    private void OnPlayerCharacterCollected()
    {
        UpdateLook();
    }

    private void UpdateLook()
    {
        _tmpValue.SetText(_inventory.GetQuantity(_kind).ToString());
    }
}
