using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventVector2 : UnityEvent<Vector2>
{

}
