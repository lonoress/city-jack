using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DebugScreen))]
public class DebugScreenEditor : BaseEditor<DebugScreen>
{
    private GUIStyle _style;

    private GUIStyle GetStyle()
    {
        if (_style == null)
        {
            _style = new GUIStyle();
            _style.alignment = TextAnchor.UpperLeft;
            _style.normal.textColor = Color.Lerp(Color.white, Color.black, 0.4f);
        }
        return _style;
    }
    
    public override void OnInspectorGUI()
    {
        GUILayout.Box(
            FormatToNewLines(
                Format("Screen.width", Screen.width),
                Format("Screen.height", Screen.height),
                Format("Screen.dpi", Screen.dpi),
                Format("Screen.brightness", Screen.brightness),
                Format("Screen.orientation", Screen.orientation),
                Format("Screen.autorotateToPortrait", Screen.autorotateToPortrait),
                Format("Screen.fullScreen", Screen.fullScreen),
                Format("Screen.safeArea", Screen.safeArea),
                Format("Screen.currentResolution.width", Screen.currentResolution.width),
                Format("Screen.currentResolution.height", Screen.currentResolution.height)
            ), GetStyle()
        );
    }

    private string Format(string title, object value)
    {
        return $"{title} - {(value == null ? "NULL" : value.ToString())}";
    }
    
}
