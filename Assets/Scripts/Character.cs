using System;
using UnityEngine;
using UnityEngine.Events;

public class Character : BaseMonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private Animator _animator;
    [SerializeField] private SOCharacter _sO;
    [SerializeField] private AudioSource _audioJump;

    private Vector3 _velocityCurrent = Vector3.zero;
    private Vector3 _velocityTarget = Vector3.zero;
    private Quaternion _rotationTarget;
    private bool _velocityUpdated = false;
    private bool _shouldJump = false;
    public readonly UnityEventCollider onTriggerEnter = new UnityEventCollider();
    
    private const float IS_GROUNDED_H = 0.06f;
    private const string ANIM_MOVEMENT_SPEED = "MovementSpeed";
    
    
    private static readonly Vector3 Vector3Zero = Vector3.zero;
    private static readonly int MovementSpeed = Animator.StringToHash(ANIM_MOVEMENT_SPEED);

    public class Event : UnityEvent<Character>{}


    private void OnTriggerEnter(Collider other)
    {
        onTriggerEnter.Invoke(other);
    }

    public void Move(Vector3 direction)
    {
        _velocityUpdated = true;
        direction.y = 0f;
        direction.Normalize();
        
        _rotationTarget = Quaternion.LookRotation(direction);

        _velocityTarget = direction * _sO.GetMovementSpeed();
    }

    public void Jump()
    {
        bool isGrounded = IsGrounded();
        if (!isGrounded) return;
        _shouldJump = true;
        _audioJump.Play();
    }

    private void FixedUpdate()
    {
        Vector3 velocity = _rigidbody.velocity;

        velocity.x = _velocityCurrent.x;
        velocity.z = _velocityCurrent.z;
        _rigidbody.velocity = velocity;

        if (_shouldJump)
        {
            _shouldJump = false;
            _rigidbody.AddForce(Vector3.up * _sO.GetJumpForce(), ForceMode.Impulse);
        }
    }

    private void LateUpdate()
    {
        if (_velocityUpdated)
        {
            _velocityUpdated = false;
            _velocityCurrent = Vector3.Lerp(_velocityCurrent, _velocityTarget, Time.deltaTime * _sO.GetVelocitySpeedIncrease());
        }
        else
        {
            _velocityTarget = Vector3Zero;
            _velocityCurrent = Vector3.Lerp(_velocityCurrent, _velocityTarget, Time.deltaTime * _sO.GetVelocitySpeedDecrease());
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, _rotationTarget, Time.deltaTime * _sO.GetRotationSpeed());
        _animator.SetFloat(MovementSpeed, _velocityCurrent.magnitude / _sO.GetAnimationMovementSpeed());
    }
    
    private bool IsGrounded()
    {
        float offset = 0.5f;
        Vector3 origin = transform.position;
        origin.y += offset;
        
        if (Physics.Raycast(origin, Vector3.down, out RaycastHit hit))
        {
            float distance = Mathf.Abs(origin.y - hit.point.y) - offset;
            return distance <= IS_GROUNDED_H;
        }
        return false;
    }
}
