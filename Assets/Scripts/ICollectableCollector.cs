
public interface ICollectableCollector
{
    public void Collect(ICollectable collectable);
}
