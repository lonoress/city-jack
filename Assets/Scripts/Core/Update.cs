using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Update
{
    private MonoBehaviour _targetBehaviour;
    private UnityAction _callback;
    private IEnumerator _routine;
    

    public Update(MonoBehaviour targetBehaviour, UnityAction callback)
    {
        _targetBehaviour = targetBehaviour;
        _callback = callback;
    }

    public void Start() {
        if(_routine != null) return;
        _routine = Handler();
        _targetBehaviour.StartCoroutine(_routine);
        Debug.Log("Update starting routine");
    }
    
    public void Stop() {    
        if(_routine == null) return;
        _targetBehaviour.StopCoroutine(_routine);
        _routine = null;
    }
    
    private IEnumerator Handler() {
        while (_routine != null)
        {
            _callback();
            yield return null;
        }
    }
}
