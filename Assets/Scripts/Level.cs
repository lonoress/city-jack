using UnityEngine;

public class Level : BaseMonoBehaviour
{
    [SerializeField] private Character _prefabCharacter;
    [SerializeField] private PlayerCamera _playerCamera;
    [SerializeField] private Player _player;


    protected override void Start()
    {
        base.Start();
        Restart();
    }

    public void Restart()
    {
        Debug.Log("Restarting level..");
        Character character = Instantiate(_prefabCharacter);
        
        _player.SetCharacter(character);
        _playerCamera.SetTarget(character.transform);
    }
}
