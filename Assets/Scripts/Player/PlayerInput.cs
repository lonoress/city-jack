using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerInput : BaseMonoBehaviour
{
    [SerializeField] private Vector2 _mouseSensitivity;
    [SerializeField] private float _zoomSensitivity;
    [SerializeField] private Player _player;
    [SerializeField] private PlayerCamera _camera;
    
#if UNITY_EDITOR
    protected override void Reset()
    {
        base.Reset();
        _mouseSensitivity = new Vector2(2000f, 2000f);
        _zoomSensitivity = 2f;
    }
#endif

    public void OnMouseMove(InputStandaloneMouse.MouseEventData evData)
    {
        if (_player.GetCharacter() == null) return;
        _camera.RotateHorizontally(evData.normalized.x * _mouseSensitivity.x);
        _camera.RotateVertically(evData.normalized.y * _mouseSensitivity.y);
    }

    public void OnMouseScroll(Vector2 delta)
    {
        if (_player.GetCharacter() == null) return;
        _camera.Zoom(delta.y * _zoomSensitivity);
    }

    public void OnJump()
    {
        _player.Jump();
    }

    public void OnMove(InputStandaloneKeyboard.MoveEventData ev)
    {
        Transform tr = _camera.GetDirectionTransform();

        Vector3 direction = Vector3.zero;
        if (ev.forward) direction += tr.forward;
        if (ev.backward) direction -= tr.forward;
        if (ev.right) direction += tr.right;
        if (ev.left) direction -= tr.right;

        _player.MoveInGlobalDirection(direction);
    }
}
